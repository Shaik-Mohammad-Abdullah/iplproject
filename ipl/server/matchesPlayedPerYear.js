const matchesPlayed = (matchesPlayedPerYear) => {
	// returns the number of matches played per year
	const result = matchesPlayedPerYear.reduce((obj, match) => {
		const year = match.season;
		if (obj[year]) {
			obj[year] += 1;
		} else {
			obj[year] = 1;
		}
		return obj;
	}, {});
	return result;
}

module.exports = matchesPlayed;
