const matchesWonPerTeam = ((matchesPlayedPerYear) => {
	// returns the number of matches played by team by season
	const result = matchesPlayedPerYear.reduce((obj, match) => {
		const year = match.season;
		const team = match.winner;
		if (obj[year]) {
			// Checking if the year exist in the object
			if (obj[year][team]) {
				obj[year][team] += 1;
			} else {
				obj[year][team] = 1;
			}
		} else {
			obj[year] = {};
		}
		return obj;
	}, {});
	return result;
});

module.exports = matchesWonPerTeam
