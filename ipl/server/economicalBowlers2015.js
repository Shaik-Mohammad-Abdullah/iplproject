// Importing the matchesFile and deliveriesFile from index.js file
const economicalBowlers = (matchesFile, deliveriesFile) => {
	const matchId = [];
	// matchId array will contain all match id's of the season 2015
	matchesFile.filter((obj) => {
		if (obj.season == 2015) {
			matchId.push(obj.id);
		}
	});

	let deliveryId1 = deliveriesFile.reduce((obj, curr) => {
		// returns in the form of {'Lakshman' : [24, 147]}
		if (matchId.includes(curr.match_id)) {
			if (!obj[curr.bowler]) {
				obj[curr.bowler] = [];
				obj[curr.bowler][0] = 1;
				obj[curr.bowler][1] = Number(curr.total_runs);
			} else {
				obj[curr.bowler][0] += 1;
				obj[curr.bowler][1] += Number(curr.total_runs);
			}
		}
		return obj;
	}, {});

	deliveryId1 = Object.entries(deliveryId1).map((element) => {
		let over = Math.floor(element[1][0] / 6);
		let ball = 0.1 * (element[1][0] % 6);
		let totalOver = over + ball;
		// Finding the exact economy rate

		element = [element[0], ((element[1][1] / totalOver).toFixed(2))]
		return element;
		// returns array containing bowler name and its economy rate
	}).sort((a, b) => a[1] - b[1]).slice(0, 10).reduce((acc, curr) => {
		acc[curr[0]] = curr[1];
		return acc;
		/* returns the sorted object containing the bowlers in a ascending order of their economy rate */
	}, {})
	return deliveryId1;
}

module.exports = economicalBowlers;
