const index = require('./index');
const fs = require('fs');

function writeToMatchesPlayed(result1) {
    const jsonData = {
        matchesPlayedPerYear: result1
    };
    const json = JSON.stringify(jsonData);
    fs.writeFile('../public/output/matchesPerYear.json', json, 'utf8', ((err) => {
        if (err) {
            console.error(err);
        }
    }));
}

function writeToMatchesWonPerTeam(result2) {
    const jsonData = {
        matchesWonPerTeamPerYear: result2
    };
    const json = JSON.stringify(jsonData);
    fs.writeFile('../public/output/matchesWonPerTeam.json', json, 'utf8', ((err) => {
        if (err) {
            console.error(err);
        }
    }));
}

function writeToEconomicalBowlers(result3) {
    const jsonData = {
        economicalBowlers: result3
    };
    const json = JSON.stringify(jsonData);
    fs.writeFile('../public/output/economicalBowlers.json', json, 'utf8', ((err) => {
        if (err) {
            console.error(err);
        }
    }));
}

function writeToExtraRuns(result4) {
    const jsonData = {
        extraRunsConceeded: result4
    };
    const json = JSON.stringify(jsonData);
    fs.writeFile('../public/output/extraRunsConceeded.json', json, 'utf8', ((err) => {
        if (err) {
            console.error(err);
        }
    }));
}

module.exports = { writeToMatchesPlayed, writeToMatchesWonPerTeam, writeToExtraRuns, writeToEconomicalBowlers };