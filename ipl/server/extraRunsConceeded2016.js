const extraRunsConceeded = (matchesFile, deliveriesFile) => {
	// returns all the match id's of the season 2016 
	const matchId = [];
	matchesFile.filter((obj) => {
		if (obj.season == 2016) {
			matchId.push(obj.id);
		}
	});
	const deliveryId = deliveriesFile.reduce((obj, curr) => {
		// returns the object containing bowling team and the extra runs made by the opponent
		if (matchId.includes(curr.match_id)) {
			if (obj[curr.bowling_team]) {
				obj[curr['bowling_team']] += Number(curr['extra_runs']);
			} else {
				obj[curr['bowling_team']] = Number(curr['extra_runs']);
			}
		}
		return obj;
	}, {});
	return deliveryId;
}

module.exports = extraRunsConceeded;
