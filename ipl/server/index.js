// Reading fs module
const fs = require('fs');

// Writing output to json format in output folder
const writeToOutput = require('./writeToOutput');

// Reading matches data
const matchesPlayed = require('./matchesPlayedPerYear');

// Reading Matches file
const matchesFile = '../data/matches.json';
const readMatches = JSON.parse(fs.readFileSync(matchesFile));

// Reading Deliveries File
const deliveriesFile = '../data/deliveries.json';
const readDeliveries = JSON.parse(fs.readFileSync(deliveriesFile));

// Calling matchesPlayed module
result1 = matchesPlayed(readMatches);
console.log(result1);
writeToOutput.writeToMatchesPlayed(result1);

// Calling matchesWon module
const matchesWon = require('./matchesWonPerTeam');
result2 = matchesWon(readMatches);
console.log(result2)
writeToOutput.writeToMatchesWonPerTeam(result2);

// Calling extraRuns module
const extraRunsConceeded = require('./extraRunsConceeded2016');
result3 = extraRunsConceeded(readMatches, readDeliveries);
console.log(result3);
writeToOutput.writeToExtraRuns(result3);

// Calling economicBowler module
const economicalBowlers = require('./economicalBowlers2015');
result4 = economicalBowlers(readMatches, readDeliveries);
console.log(result4);
writeToOutput.writeToEconomicalBowlers(result4);

module.exports = { result4, result3, result2, result1 };
