// Importing csvtojson and fs module
const csv = require('csvtojson');
const fs = require('fs')

// Reading Matches file
const matchesCsvFile = '../data/matches.csv'
const matchesFile = '../data/matches.json';

// Reading Deliveries File
const deliveriesCsvFile = '../data/deliveries.csv'
const deliveriesFile = '../data/deliveries.json';

// Converting csv module to json module for deliveries.csv file
csv()
    .fromFile(deliveriesCsvFile)
    .then((element) => {
        fs.writeFile(deliveriesFile, JSON.stringify(element, null, 4), "utf-8", (error) => {
            console.log(error);
        });
    });

// Converting csv module to json module for matches.csv file
csv()
    .fromFile(matchesCsvFile)
    .then((element) => {
        fs.writeFile(matchesFile, JSON.stringify(element, null, 4), "utf-8", (error) => {
            console.log(error);
        });
    });
