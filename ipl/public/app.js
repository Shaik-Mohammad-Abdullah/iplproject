function fetchAndVisualizeData() {
	fetch('./output/matchesPerYear.json').then((r) => r.json()).then((data) => visualizeMatchesPlayedPerYear(data.matchesPlayedPerYear));
	fetch('./output/economicalBowlers.json').then((r) => r.json()).then((data) => visualizeEconomicalBowlers(data.economicalBowlers));
	fetch('./output/extraRunsConceeded.json').then((r) => r.json()).then((data) => visualizeExtraRunsConceeded(data.extraRunsConceeded));
	fetch('./output/matchesWonPerTeam.json').then((r) => r.json()).then((data) => visualizeMatchesWonPerTeam(data.matchesWonPerTeamPerYear));
}

fetchAndVisualizeData();

function visualizeMatchesPlayedPerYear(matchesPlayedPerYear) {
	const seriesData1 = [];
	for (let year in matchesPlayedPerYear) {
		seriesData1.push([year, matchesPlayedPerYear[year]])
	}
	Highcharts.chart('matches-played-per-year', {
		chart: {
			type: 'column'
		},
		title: {
			text: 'Matches Played Per Year'
		},
		xAxis: {
			type: 'category',
			labels: {
				rotation: -45,
				style: {
					fontSize: '13px',
					fontFamily: 'Verdana, sans-serif'
				}
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Number of Matches'
			}
		},
		legend: {
			enabled: false
		},
		series: [{
			data: seriesData1,
			dataLabels: {
				enabled: true,
				rotation: -90,
				color: '#FFFFFF',
				align: 'right',
				format: '{point.y:.1f}', // one decimal
				y: 10, // 10 pixels down from the top
				style: {
					fontSize: '13px',
					fontFamily: 'Verdana, sans-serif'
				}
			}
		}]
	});
}




function visualizeEconomicalBowlers(economicalBowlers) {
	const seriesData2 = [];
	for (let bowler in economicalBowlers) {
		seriesData2.push([bowler, parseFloat(economicalBowlers[bowler])]);
	}
	Highcharts.chart('economical-bowlers-2015', {
		chart: {
			type: 'column'
		},
		title: {
			text: 'Top 10 Economical Bowlers'
		},
		xAxis: {
			type: 'category',
			labels: {
				rotation: -45,
				style: {
					fontSize: '13px',
					fontFamily: 'Verdana, sans-serif'
				}
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Economy rate'
			}
		},
		legend: {
			enabled: false
		},
		series: [{
			data: seriesData2,
			dataLabels: {
				enabled: true,
				rotation: -90,
				color: '#FFFFFF',
				align: 'right',
				format: '{point.y:.1f}', // one decimal
				y: 10, // 10 pixels down from the top
				style: {
					fontSize: '13px',
					fontFamily: 'Verdana, sans-serif'
				}
			}
		}]
	});
}

function visualizeExtraRunsConceeded(extraRunsConceeded) {
	const seriesData3 = [];
	for (let team in extraRunsConceeded) {
		seriesData3.push([team, extraRunsConceeded[team]])
	}
	Highcharts.chart('extra-runs-conceeded-2016', {
		chart: {
			type: 'column'
		},
		title: {
			text: 'Extra Runs Conceeded in 2016'
		},
		xAxis: {
			type: 'category',
			labels: {
				rotation: -45,
				style: {
					fontSize: '13px',
					fontFamily: 'Verdana, sans-serif'
				}
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Extra Runs Conceeded'
			}
		},
		legend: {
			enabled: false
		},
		series: [{
			data: seriesData3,
			dataLabels: {
				enabled: true,
				rotation: -90,
				color: '#FFFFFF',
				align: 'right',
				format: '{point.y:.1f}', // one decimal
				y: 10, // 10 pixels down from the top
				style: {
					fontSize: '13px',
					fontFamily: 'Verdana, sans-serif'
				}
			}
		}]
	});
}

function visualizeMatchesWonPerTeam(matchesWonPerTeamPerYear) {
	const years = Object.keys(matchesWonPerTeamPerYear);
	const seriesData4 = Object.values(matchesWonPerTeamPerYear);
	const yearsData = seriesData4.reduce((accumulator, object) => {
		Object.keys(object).forEach((element) => {
			if (!accumulator.includes(element)) {
				accumulator.push(element);
			}
		});
		return accumulator;
	}, []).sort();
	Highcharts.chart('matches-won-per-team-per-year', {
		chart: {
			type: 'bar',
			height: yearsData.length * years.length * 10
		},
		title: {
			text: 'Matches Won per team per year'
		},
		xAxis: {
			categories: yearsData,
			title: {
				text: null
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'Number of Matches Won',
				align: 'high'
			},
			labels: {
				overflow: 'justify'
			}
		},
		plotOptions: {
			bar: {
				dataLabels: {
					enabled: true,
				},
				ignoreNulls: true,
			},
			series: {
				grouping: true,
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -0,
			y: +150,
			floating: true,
			borderWidth: 1,
			backgroundColor:
				Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
			shadow: true
		},
		credits: {
			enabled: false
		},
		series: yearsData.reduce((accumulator, value) => {
			const winsEachTeam = seriesData4.map((object) => {
				return object[value] || null;
			});
			accumulator.push({
				name: value,
				data: winsEachTeam
			});
			return accumulator;
		}, [])
	});
}
