# IPL Data Project

## Steps to follow

1. After cloning the folder, Do `npm install`. This command will install all the dependancies locally
2. Run `node csvReadAndWrite.js` to write the output files in json format
3. Now, do `npm start` to run the web server. If the folder tab opens, then click on the `ipl` folder. And, then click on the `public` folder
4. You get the output with high charts